BUILD 329. ĐƠN HÀNG LOGISTIC - http://redmine.ipicorp.co/issues/45889
Author: Phạm Hồng Ân - FE

1. Nhu cầu
    - TTPP thường thấy được đơn của TTPP Logistic → Dựng mới UI Đơn hàng Logistic
   
2. Giải pháp của CLIENT
   1. Tool: TTPP
   2. Nội dung
      - Cách truy cập: 
        + http://proipittpp.ipicorp.co/order/logistic
        + Submenu "Phiếu yêu cầu" → click chọn "Đơn hàng Logistic"
        
      - Tiêu đề page: Danh sách đơn hàng Logistic
      - Tìm kiếm theo mã đơn, mã và tên của ĐLC1
      
      - Bộ lọc bao gồm:
        + lọc từ ngày đến ngày
        + lọc theo chi nhánh: center_tower/get_list_agency_by_center_tower_id
        + lọc theo trạng thái (đơn hàng): center_tower/get_list_status
        + lọc theo ngành hàng: center_tower/get_all_cate
        
      - Table gồm 12 cột nội dung:
        + STT
        + Mã đơn: code
        + Loại đơn: order_type_name
        + Ngày tạo: create_date
        + Tên chi nhánh: agency_name
        + Đại lý cấp 1: user_agency_name
        + Trạng thái: status_name
        + Đồng bộ SAP: sync_sap_status (0: Chưa đồng bộ | 1: Đã đồng bộ | -1: Đồng bộ thất bại)
        + Đồng bộ TMS: sync_tms_status (0: Chưa đồng bộ | 1: Đã đồng bộ | -1: Đồng bộ thất bại)
        + Kho giao hàng: lswarehouse
        + Tổng tiền: total_money
        + Tổng trọng lượng: total_weight
        
      - API sử dụng: order_child_s1_receipt/get_list_order_logitics
        + params: start_date, page, key, status_id, end_date, agency_id, company_id, order_type (luôn = 0)
        + API sẽ gọi lại khi 1 trong những params trên thay đổi
        
      - Thao tác khác: Khi click vào mỗi đơn hàng
        + Nếu status_id của đơn hàng = 26 → redirect đến page: "Chi tiết Phiếu yêu cầu..."
        + Nếu status_id của đơn hàng != 26 → redirect đến page: "Điều phối đơn..."
