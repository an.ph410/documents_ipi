# BUILD 279. QUẢN LÝ MỐI QUAN HỆ NÔNG DÂN - http://redmine.ipicorp.co/issues/44139
#### Author: Phạm Hồng Ân - FE


## I/ Nhu cầu
- View, export excel ```mối quan hệ của nông dân với đại lý C1, c2 và Ruộng```
- Lưu thêm ```tên ruộng, mã ruộng``` vào thông tin đơn hàng nông dân ở tool admin, chi nhánh

## II/ Giải pháp
### 1/ Thêm tính năng mới __Quản lý Mối quan hệ Nông dân__

#### 1.1/ Truy cập
- __Submenu__ Nông dân & Ruộng → chọn Quản lý MQG Nông dân
- __Path__: ```/farmer/relationship```

#### 1.2/ Danh sách Mối quan hệ Nông dân
- __Title__: Quản lý Mối quan hệ Nông dân
- __API__: ```/relationship_farmer/get_all_relationship_farmer```
- __Params__

|STT|params|defined_key|note|
|:---------------:|:------------------:|:---------------:|:----------------:|
|1| search| | Tìm kiếm theo mã ND, tên ND
|2| agency_ids| | multichoice, api: ```agency/get_all_agency_active```
|3| user_status| ```2- tất cả, 1- hoạt động, 0- ngừng hoạt động```| Trạng thái đại lý
|4| user_assign_status| ```2- tất cả, 1- đã gán, 0- chưa gán```|Trạng thái gán Đại lý
|5| field_assign_status| ```2- tất cả, 1- đã gán, 0- chưa gán```| Trạng thái gán Ruộng
|6| farmer_type| ```2- tất cả, 1- nRMS, 0- LTG```| Loại nông dân
|7| farmer_status|``` 2- tất cả, 1- hoạt động, 0- ngừng hoạt động```| Trạng thái nông dân
|8| material_region| ```2- tất cả, 1- trong, 0- ngoài```| Vùng nhiên liệu
|9| page| | 
|10| size| |luôn bằng 10
- __Thông tin columns__

|STT|Tên cột|key|type|note|
|:---------------:|:------------------:|:---------------:|:----------------:|:----------------:|
|1|STT|`stt`|int|-
|2|Mã nông dân|`farmer_code`|string|Thông tin ND
|3|Tên nông dân|`farmer_name`|string|Thông tin ND
|4|Chi nhánh|`farmer_agency`|string|Thông tin ND
|5|Loại nông dân|`farmer_type`|string|Thông tin ND
|6|Trạng thái nông dân|`farmer_status`|string|Thông tin ND
|7|Số điện thoại|`farmer_phone`|string|Thông tin ND
|8|Số CMND/CCCD|`farmer_cmnd`|string|Thông tin ND
|9|Ngày cấp|`farmer_cmnd_ngaycap`|int|Thông tin ND
|10|Ngày sinh|`farmer_ngaysinh`|int|Thông tin ND
|11|Nơi cấp|`farmer_noicap`|string|Thông tin ND
|12|Địa chỉ|`farmer_address`|string|Thông tin ND
|13|Tỉnh/Thành|`farmer_city`|string|Thông tin ND
|14|Quận/Huyện|`farmer_district`|string|Thông tin ND
|15|Phường/Xã|`farmer_ward`|string|Thông tin ND
|16|Tên nhân viên|`saleman_name`|string|Thông tin NV trực thuộc
|17|Mã nhân viên|`saleman_code`|string|Thông tin NV trực thuộc
|18|Vùng nhiên liệu|`material_region`|string|Thông tin Ruộng
|19|Tên ruộng|`field_name`|string|Thông tin Ruộng
|21|Trạng thái ruộng|`field_status`|string|Thông tin Ruộng
|22|Tỉnh/Thành|`field_city`|string|Thông tin Ruộng
|23|Quận/Huyện|`field_district`|string|Thông tin Ruộng
|24|Phường/Xã|`field_ward`|string|Thông tin Ruộng
|25|Trạng thái gán Ruộng|`field_assign_status`|string|Thông tin Ruộng
|26|Tên đại lý C1|`s1_name`|string|Thông tin ĐLC1
|27|Mã đại lý C1|`s1_code`|string|Thông tin ĐLC1
|28|Trạng thái đại lý C1|`s1_status`|string|Thông tin ĐLC1
|29|Chi nhánh|`s1_agency`|string|Thông tin ĐLC1
|30|Tỉnh/Thành|`s1_city`|string|Thông tin ĐLC1
|31|Quận/Huyện|`s1_district`|string|Thông tin ĐLC1
|32|Phường/Xã|`s1_ward`|string|Thông tin ĐLC1
|33|Trạng thái gán ĐLC1|`s1_assign_status`|string|Thông tin ĐLC1
|32|Tên đại lý C2|`s2_name`|string|Thông tin ĐLC2
|33|Mã đại lý C2|`s2_code`|string|Thông tin ĐLC2
|34|Trạng thái đại lý C2|`s2_status`|string|Thông tin ĐLC2
|35|Chi nhánh|`s2_agency`|string|Thông tin ĐLC2
|36|Tỉnh/Thành|`s2_city`|string|Thông tin ĐLC2
|37|Quận/Huyện|`s2_district`|string|Thông tin ĐLC2
|38|Phường/Xã|`s2_ward`|string|Thông tin ĐLC2
|39|Trạng thái gán ĐLC2|`s2_assign_status`|string|Thông tin ĐLC2

#### 1.2/ Danh sách Mối quan hệ Nông dân
- __API__: ```/export_excel/export_relationship_farmer_c1_c2_field```
- __Params__

|STT|params|defined_key|note|
|:---------------:|:------------------:|:---------------:|:----------------:|
|1| search| | Tìm kiếm theo mã ND, tên ND
|2| agency_ids| | multichoice, api: ```agency/get_all_agency_active```
|3| user_status| ```2- tất cả, 1- hoạt động, 0- ngừng hoạt động```| Trạng thái đại lý
|4| user_assign_status| ```2- tất cả, 1- đã gán, 0- chưa gán```|Trạng thái gán Đại lý
|5| field_assign_status| ```2- tất cả, 1- đã gán, 0- chưa gán```| Trạng thái gán Ruộng
|6| farmer_type| ```2- tất cả, 1- nRMS, 0- LTG```| Loại nông dân
|7| farmer_status|``` 2- tất cả, 1- hoạt động, 0- ngừng hoạt động```| Trạng thái nông dân
|8| material_region| ```2- tất cả, 1- trong, 0- ngoài```| Vùng nhiên liệu

### 2/ Update UI & API __Chi tiết đơn hàng Nông dân__
- __API__: 
  - admin: `/order_farmer/admin/get_detail_order_farmer`
  - chi nhánh: `/order_farmer/get_detail_order_farmer`
- __Params__: `order_code` - mã đơn hàng
- __Update field__: 

|title|key|
|:---------------:|:------------------:|
|Mã ruộng|farmer_field_code
|Tên ruộng|farmer_field_name
